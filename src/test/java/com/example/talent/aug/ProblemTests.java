package com.example.talent.aug;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ProblemTests {
    @DisplayName("从aaabcccd中删除2个字符")
    @Test
    public void test_compress() {
        Problem problem = new Problem();
        assertEquals(4, problem.getLengthOfOptimalCompression("aaabcccd", 2), "删除2个");
    }


    @DisplayName("从aabbaa中删除2个字符")
    @Test
    public void test_compress_02() {
        Problem problem = new Problem();
        assertEquals(2, problem.getLengthOfOptimalCompression("aabbaa", 2), "删除2个");
    }

    @DisplayName("不从aaaaaaaaaaa中删除字符")
    @Test
    public void test_compress_03() {
        Problem problem = new Problem();
        assertEquals(3, problem.getLengthOfOptimalCompression("aaaaaaaaaaa", 0), "不删除");
    }


    @DisplayName("从'中人人中'中删除2个字符")
    @Test
    public void test_compress_04() {
        Problem problem = new Problem();
        assertEquals(2, problem.getLengthOfOptimalCompression("中人人中", 2), "删除2个");
    }

}
