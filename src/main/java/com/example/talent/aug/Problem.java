package com.example.talent.aug;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

public class Problem {
    // 游码压缩
    // s不含数字
    // 1 <= s.length <= 100, 0 <= remove << s.length
    // 从s中删除k个字符，再将剩下的字符做游码编码(Run Length Code),
    // 如AAAAABBBBCCCD => A5B4C3D
    // 求压缩完成后的字符的长度
    public int getLengthOfOptimalCompression(String s, int remove) {
    	
    	if(remove > 0) {
    		s = deleteMixChar(s,remove);
    	}
    	
    	String STR_FORMAT = "";
        for (int i = 0; i < remove; i++) {
            STR_FORMAT = STR_FORMAT + "";
        }

        DecimalFormat df = new DecimalFormat(STR_FORMAT);
        StringBuffer sbuffer = new StringBuffer();

        int length = s.length();

        char c = s.charAt(0);
        int current_size = 0;
        for (int i = 0; i <= length; i++) {
        	
            if (i == length) {
            	if(current_size > 1) {
            		 sbuffer.append(c).append(df.format(current_size));
            	}else {
            		 sbuffer.append(c);
            	}
                break;
            }
            char c1 = s.charAt(i);

            if (c == c1) current_size++;
            else {
            	if(current_size > 1) {
            		 sbuffer.append(c).append(df.format(current_size));
            	}else {
            		 sbuffer.append(c);
            	}
                current_size = 1;
                c = c1;
            }
        }
		return sbuffer.length();
    }
    
    
    private static String deleteMixChar(String s,int remove) {
    	int tmoRemove = remove;
        //创建一个hashMap存储，存储字符串中的字符和其出现的次数
        HashMap<Character, Integer> hm = new HashMap<>();
        char[] ch = s.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            //如果hm集合中已经有这个键，则存入时直接将值加1
            if(hm.containsKey(ch[i])){
                hm.put(ch[i], hm.get(ch[i])+1);
            }else{
                hm.put(ch[i], 1);
            }
        }
        Collection<Integer> values = hm.values();
        Integer min = Collections.min(values);
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < ch.length; i++) {
        	if(ch[i] > 0 && hm.get(ch[i])==min){
        		if(hm.get(ch[i]) > remove) {
        			hm.put(ch[i], hm.get(ch[i]) - remove);
        			remove = 0;
        			break;
        		}else {
        			remove = remove - hm.get(ch[i]);
        			hm.put(ch[i], 0);
        		}
        	}
        	if(remove == 0) {
        		break;
        	}
        }
        for(Object obj : hm.keySet()){
        	for(int j =1;j<= hm.get(obj);j++) {
        		str.append(obj);
        	}
        }
        if(remove > 0) {
        	return deleteMixChar(str.toString(),remove);
        }
        return str.toString();
    }
    
}

