package com.example.talent.aug;



import java.awt.RenderingHints.Key;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.html.parser.Parser;

public class Solution {

    public String[] rename(String[] names) {
    	
    	//先存储原先的数组值
    	String [] tmpNames = names.clone();
    	Matcher matherOne = null;
    	Matcher matherTwo = null;
    	int index = 0;
    	String strTmp = "";
    	Map<String,String> mapOne=new HashMap<String, String>();
    	Map<String,String> mapTwo=new HashMap<String, String>();
    	ArrayList<String> arraylist_index = new ArrayList<String>();
    	Pattern pattern1 = Pattern.compile("((\\(\\d*\\))+)$");
    	for(int i = 0;i<names.length;i++) {
    		matherOne = pattern1.matcher(names[i]);
    		if(matherOne.find()) {
    			//存储一个()中得值
    			if(!matherOne.group(0).equals(matherOne.group(2))) {
    				mapOne.put(i+"", matherOne.group(0).replace("(","").replace(")",""));
    				names[i]  = matherOne.replaceAll("").toString();
    			}
    			
    		}
    		 
    	}
    	//将所有的后缀去掉用来计算重复项有几种
    	for(int i = 0;i<tmpNames.length;i++) {
    		matherOne = pattern1.matcher(tmpNames[i]);
    		if(matherOne.find()) {
    			tmpNames[i] = matherOne.replaceAll("").toString();
    		}
    		 
    	}
    	//计算所有整个数组的重复项
    	String [] oneArray = oneClear(tmpNames);
    	
    	for(int i = 0;i<oneArray.length;i++) {
    		mapTwo.put(oneArray[i], "-1");
    	}
    	//计算每个重复项的下标
    	for(int i = 0;i<oneArray.length;i++) {
    		for(int j = 0;j<names.length;j++) {
				if(oneArray[i].equals(names[j])) {
					index = Integer.parseInt(mapTwo.get(oneArray[i])) + 1;
					mapTwo.put(oneArray[i], index+"");
					
        		}
    		
        	}
    		
    	}
    	//判断是否有()重复的将其去掉后缀，需要排序
    	for(int i=0;i<names.length;i++) {
    		for(int j=0;j<names.length;j++) {
    			if(i != j) {
    				if(names[i].equals(names[j])) {
    					matherOne = pattern1.matcher(names[j]);
    		    		if(matherOne.find()) {
    		    			//存储一个()中得值
    		    			if(matherOne.group(0).equals(matherOne.group(2))) {
    		    				names[j]  = matherOne.replaceAll("").toString();
    		    			}
    		    		}
    				}
    			}
    		}
    	}
    	
    	String strTmpTwo = "";
    	Pattern pattern2 = Pattern.compile("(\\(\\d*\\))$");
    	ArrayList<String> arrayList = new ArrayList<String>();
    	for(int i =0;i<oneArray.length;i++) {
    		arrayList.clear();
    		for(int j = 0;j<names.length;j++) {
    			matherOne = pattern1.matcher(names[j]);
        		if(matherOne.find()) {
        			strTmpTwo = matherOne.replaceAll("").toString();
        			if(oneArray[i].equals(strTmpTwo)) {
        				matherTwo = pattern2.matcher(names[j]);
                		if(matherTwo.find()) {
                			if(matherTwo.groupCount() == 1) {
                				//if(Integer.parseInt(matherTwo.group(0).replace("(","").replace(")","")) <= Integer.parseInt(mapTwo.get(oneArray[i]).toString())) {
                					if(!arrayList.contains(matherTwo.group(0).replace("(","").replace(")",""))) {
                						arrayList.add(matherTwo.group(0).replace("(","").replace(")",""));
                					}
                				//}
                			}
                		}
        			}
        		}
    		}
    		
    		index = 1;
    		boolean isFirst = false;
    		for(int j = 0;j<names.length;j++) {
    			if(oneArray[i].equals(names[j])) {
    				if(!isFirst) {
    					isFirst = true;
    					continue;
    				}
    				//先验证重复原本下标
    				for(int mm=0;mm<arrayList.size();mm++) {
    					if(Integer.parseInt(arrayList.get(mm)) == index) {
    						index = index + 1;
        				}
    				}
    				//验证是否重复
    				names[j] = names[j] + "("+index+")";
        			index = index +1 ;
    			}
    		}
    	}
		return names;

    }
    
    //方法一：通过list去重
    public String [] oneClear(String [] arrStr) {
        ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i<arrStr.length; i++) {
            if(!list.contains(arrStr[i])) {
                list.add(arrStr[i]);
            }
        }
        //返回一个包含所有对象的指定类型的数组
        String[] newArrStr =  list.toArray(new String[1]);
       return newArrStr;
    }
}
